// -*- mode: js2; indent-tabs-mode: nil; js2-basic-offset: 4 -*-

//    Hide first child in the left box
//    Puts icon and title of the current window in the gnome shell top panel
//
//    ----------------------------------------------------------------------
//    Copyright © 2018  Pellegrino Prevete
//
//    All rights reserved
//    ----------------------------------------------------------------------
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Main = imports.ui.main;

function init(metadata) {
}

function enable() {
    _child = Main.panel._leftBox.get_child_at_index(0);
    Main.panel._leftBox.remove_actor(_child);
}

function disable() {
    Main.panel._leftBox.insert_child_at_index(_child, 0);
}

# Hide first child in panel

[![License: GPL v3+](https://img.shields.io/badge/license-GPL%20v3%2B-blue.svg)](http://www.gnu.org/licenses/gpl-3.0) 
[![Javascript Support](https://img.shields.io/badge/gjs-1.50.x-orange.svg)](https://gitlab.gnome.org/GNOME/gjs/wikis/Home)

![Hide first child in panel](screenshot.png)

*Hide first child in left box* is a GNOME Shell extension with an ambiguous name that hides the first widget (the first of the left box!) in your Gnome Shell panel, so if you did not modify your panel, it would hide the Activities button.

Actually I wrote this because I could not find the code of the others 'hide activities'.

## Installation

Just type these commands in your terminal:

    git clone https://gitlab.com/tallero/hide-first-child-in-left-box
    cp -r hide-first-child-in-left-box/hide-first-child-in-left-box\@prevete.ml ~/.local/share/gnome-shell/extensions
    gnome-shell-extension-tool -e hide-first-child-in-left-box@prevete.ml

If still not working, restart `gnome-shell` pressing `Alt+F2` and then executing `r`.

## About

This program is licensed under [GNU General Public License v3 or later](https://www.gnu.org/licenses/gpl-3.0.en.html) by [Pellegrino Prevete](http://prevete.ml). If you find this program useful, consider offering me a [beer](https://patreon.com/tallero), a new [computer](https://patreon.com/tallero) or a part time remote [job](mailto:pellegrinoprevete@gmail.com) to help me pay the bills.
